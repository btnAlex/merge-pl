const pkg = require('./package')

module.exports = {
    "apiPath": "stubs/api",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        "merge-auth.main": "/merge",
        "link.merge-auth.login": "/login",
        "link.merge-auth.sign-up": "/sign-up"
    }
}
