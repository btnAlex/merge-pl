import React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import styled from '@emotion/styled';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Typography from '@mui/material/Typography';
import Stack from '@mui/material/Stack';
import i18next from 'i18next';
import { Link } from 'react-router-dom';
import { URLs } from '../../__data__/urls';

const Wrapper = styled.div`
    background-color: #fff;
    padding: 24px;
    display: block;
    max-width: 300px;
    max-height: 500px;
    margin: auto;
    margin-top: 10%;
    box-shadow: 0 0 20px 0px #00000033;
`;


const SignUp = () => {
    return(
        
        <Wrapper>
            <Stack spacing={4}>
                
                <Typography 
                    variant="h4" 
                    gutterBottom 
                >
                    {i18next.t('merge.signup.sorry')}
                </Typography>

               

                <Typography variant="subtitle1" gutterBottom component="div">
                     <Link to={URLs.login.url}>{i18next.t('merge.back')}</Link>
                </Typography>
            </Stack>
        </Wrapper>
    )
}

export default SignUp;
