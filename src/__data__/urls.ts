import { getNavigationsValue } from '@ijl/cli';

const appurl = getNavigationsValue('merge-auth.main');

export const URLs = {
    main: appurl,
    login: {
        url: `${appurl}${getNavigationsValue('link.merge-auth.login')}`
    },
    signUp: {
        url: `${appurl}${getNavigationsValue('link.merge-auth.sign-up')}`
    }
}

