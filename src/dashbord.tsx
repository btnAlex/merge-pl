import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import SignIn from './views/sign-in';
import SignUp from './views/sign-up';
import {URLs} from './__data__/urls'; 

const Dashbord = () => {
    return (
        <Switch>
            <Route path={URLs.main} exact>
                <Redirect to={URLs.login.url}/>
            </Route>

            <Route path={URLs.login.url}>
                <SignIn/>
            </Route>

            <Route path={URLs.signUp.url}>
                <SignUp/>
            </Route>
        </Switch>
    )
        
}

export default Dashbord;