import React from 'react';
import Dashboard from './dashbord';
import { Router } from 'react-router-dom';
import { getHistory } from '@ijl/cli';

const history = getHistory();

const App = () => {
    return (
        <Router history={history}>
            <Dashboard/>
        </Router>
    )
}

export default App;